TEMPLATE = lib
TARGET = ziplib

QT -= core gui
CONFIG += static create_prl link_prl
CONFIG += c++11 object_parallel_to_source
CONFIG += warn_off
CONFIG += no_batch

macx {
    QMAKE_CFLAGS    = -fPIC -Wno-enum-conversion -O3
    QMAKE_CXXFLAGS  = -fPIC -std=c++11 -O3
}

CONFIG(debug, debug|release) {
    SUBDIR = debug
}
CONFIG(release, debug|release) {
    SUBDIR = release
}

OBJECTS_DIR = obj/$${SUBDIR}

DESTDIR = $${SUBDIR}

include(ziplib.pri)
