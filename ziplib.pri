SOURCES += $$files(Source/ZipLib/*.cpp, false) \
           $$files(Source/ZipLib/detail/*.cpp, false) \
           $$files(Source/ZipLib/extlibs/bzip2/*.c, false) \
           $$files(Source/ZipLib/extlibs/zlib/*.c, false) \

win32 {
    SOURCES += $$files(Source/ZipLib/extlibs/lzma/*.c, false) \
}
macx {
    SOURCES += $$files(Source/ZipLib/extlibs/lzma/unix/*.c, false) \
}

